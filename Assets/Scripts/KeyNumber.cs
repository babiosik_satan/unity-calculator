﻿using UnityEngine;

public class KeyNumber : KeyPrimary
{
    [SerializeField] 
    private int number;

    override public void Click() {
        calculator.Input(number);
    }
}