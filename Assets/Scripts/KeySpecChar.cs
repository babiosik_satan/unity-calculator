﻿using UnityEngine;

public class KeySpecChar : KeyPrimary
{
    [SerializeField] 
    private Calculator.SpecChar specChar;

    override public void Click() {
        calculator.Input(specChar);
    }
}
