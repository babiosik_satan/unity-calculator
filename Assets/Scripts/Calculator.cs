﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Calculator : MonoBehaviour
{
    public enum SpecChar { Clear, Backspace, Dot, Equal, Plus, Minus, Multiple, Divide, Sqr, Sqrt }

    [SerializeField]
    private Text equation; //Text for view all equation
    [SerializeField]
    private Text resultText; //Text for view result of all
    [SerializeField]
    private List<double> numbers; // list all numbers in equation
    [SerializeField]
    private List<SpecChar> operations; // list all operations in equation
    
    private bool isLastMinus = false;
    public int numAfterDot = -1; // count char after dot in float number. Integer has -1

    private void Start() {
        numbers = new List<double>{ 0 };
        operations = new List<SpecChar>();
        UpdateEquation();
    }

    public void Input(int num) {
        double oldNum = numbers[numbers.Count - 1];
        if (numAfterDot == -1) // Integer number
            numbers[numbers.Count - 1] = oldNum * 10 + num * (isLastMinus ? -1 : 1);
        else if (numAfterDot < 6) // Float number
            numbers[numbers.Count - 1] = oldNum + num / Mathf.Pow(10f, (++numAfterDot)) * (isLastMinus ? -1 : 1);

        UpdateEquation();
    }
    public void Input(SpecChar specChar) {
        // any specChar without Dot and Backspace ending input float number
        if (specChar != SpecChar.Dot && specChar != SpecChar.Backspace) {
            isLastMinus = false;
            numAfterDot = -1;
        }
        switch(specChar) {
        case SpecChar.Clear://Clear equation
            Start();
            break;
        case SpecChar.Backspace://Erase last char
            double oldNum = numbers[numbers.Count - 1];
            if (oldNum == 0) {// Remove last element
                if (numbers.Count > 1) {
                    numbers.RemoveAt(numbers.Count - 1);
                    operations.RemoveAt(operations.Count - 1);
                    isLastMinus = numbers[numbers.Count - 1] < 0;
                }
                break;
            } 
            // Check have floating part
            if (oldNum % 1 != 0 && numAfterDot == -1)
                numAfterDot = (oldNum).ToString().Split('.')[1].Length;
            if (numAfterDot == -1) {// Remove Integer part
                if (isLastMinus)
                    numbers[numbers.Count - 1] = System.Math.Ceiling(oldNum / 10f);
                else
                    numbers[numbers.Count - 1] = System.Math.Floor(oldNum / 10f);
            } else if (numAfterDot == 0) // Remove Dot
                numAfterDot = -1;
            else { // Remove floating part
                if (isLastMinus)
                    oldNum = System.Math.Ceiling(oldNum * System.Math.Pow(10f, numAfterDot - 1));
                else
                    oldNum = System.Math.Floor(oldNum * System.Math.Pow(10f, numAfterDot - 1));
                oldNum = oldNum / System.Math.Pow(10f, numAfterDot - 1);
                numbers[numbers.Count - 1] = oldNum;
                numAfterDot--;
            }
            break;
        case SpecChar.Dot://Start enter floating part number
            numAfterDot = numAfterDot == -1 ? 0 : numAfterDot;
            break;
        case SpecChar.Minus:
            if (numbers[numbers.Count - 1] == 0) // Set current value negative
                isLastMinus = true;
            else { // Set minus as next operation
                operations.Add(specChar);
                numbers.Add(0);
            }
            break;
        case SpecChar.Plus:
            if (numbers[numbers.Count - 1] == 0) // Set current value positive
                isLastMinus = false;
            else { // Set plus as next operation
                operations.Add(specChar);
                numbers.Add(0);
            }
            break;
        // For *, /, ^ and sqrt only set as next operation
        case SpecChar.Multiple:
        case SpecChar.Divide:
        case SpecChar.Sqr:
        case SpecChar.Sqrt:
            numbers.Add(0);
            operations.Add(specChar);
            break;
        }
        
        UpdateEquation();
    }

    private void UpdateEquation() {
        equation.text = "";
        // Print all prev elements
        for(int i = 0; i < numbers.Count - 1; i++)
            equation.text += $"{numbers[i].ToString()} {SpecCharToString(operations[i])} ";
        // Print current editable element
        equation.text += PrepareLastElement();
        // Print result
        resultText.text = CollapseResult().ToString();
    }
    private string PrepareLastElement() {
        string lastElement = "";
        // Set format output value. Like 0.000
        string lastNumberFormat = "0";
        if (numAfterDot > -1) {
            lastNumberFormat += ".";
            for(int i = 0; i < numAfterDot; i++)
                lastNumberFormat += "0";
        }
        // Show negative zero
        if (isLastMinus && numbers[numbers.Count - 1] == 0)
            lastElement += "-";
        lastElement += numbers[numbers.Count - 1].ToString(lastNumberFormat);
        // Show dot after integer part when no floating part but have dot
        if (lastNumberFormat.Length == 2)
            lastElement += ".";
        return lastElement;
    }
    private double CollapseResult() {
        List<double> numberList = new List<double>(numbers);
        List<SpecChar> operationList = new List<SpecChar>(operations);
        // Collapse 3rd level operators like ^ and sqrt
        for(int i = 0; i < operationList.Count;) {
            switch(operationList[i]) {
            case SpecChar.Sqr:
                numberList[i] = System.Math.Pow(numberList[i], numberList[i + 1]);
                break;
            case SpecChar.Sqrt:
                numberList[i] = System.Math.Pow(numberList[i], 1 / numberList[i + 1]);
                break;
            default: i++; continue;
            }
            numberList.RemoveAt(i + 1);
            operationList.RemoveAt(i);
        }
        // Collapse 3rd level operators like * and /
        for(int i = 0; i < operationList.Count;) {
            switch(operationList[i]) {
            case SpecChar.Multiple:
                numberList[i] *= numberList[i + 1];
                break;
            case SpecChar.Divide:
                if (numberList[i + 1] == 0)
                    return double.NaN;
                numberList[i] /= numberList[i + 1];
                break;
            default: i++; continue;
            }
            numberList.RemoveAt(i + 1);
            operationList.RemoveAt(i);
        }
        // Collaspe left operators like + and -
        while(operationList.Count > 0) {
            double leftNum = numberList[0];
            double rightNum = numberList[1];
            leftNum += rightNum * (operationList[0] == SpecChar.Minus ? -1 : 1);
            numberList[0] = leftNum;
            numberList.RemoveAt(1);
            operationList.RemoveAt(0);
        }
        return numberList[0];
    }

    // Convert operators to string
    private string SpecCharToString(SpecChar specChar) {
        switch(specChar) {
        case SpecChar.Minus:
            return "-";
        case SpecChar.Plus:
            return "+";
        case SpecChar.Multiple:
            return "*";
        case SpecChar.Divide:
            return "/";
        case SpecChar.Sqr:
            return "^";
        case SpecChar.Sqrt:
            return "^1/";
        default:
            return "!Error!";
        }
    }
}