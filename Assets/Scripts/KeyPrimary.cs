using UnityEngine;
using UnityEngine.UI;

public class KeyPrimary : MonoBehaviour {
    [SerializeField]
    protected Calculator calculator;

    private void Start() {
        GetComponent<Button>().onClick.AddListener(Click);
    }

    public virtual void Click() {}
}